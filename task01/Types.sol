// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

interface IDataTypesPractice {
    function getInt256() external view returns(int256);
    function getUint256() external view returns(uint256);
    function getIint8() external view returns(int8);
    function getUint8() external view returns(uint8);
    function getBool() external view returns(bool);
    function getAddress() external view returns(address);
    function getBytes32() external view returns(bytes32);
    function getArrayUint5() external view returns(uint256[5] memory);
    function getArrayUint() external view returns(uint256[] memory);
    function getString() external view returns(string memory);

    function getBigUint() external pure returns(uint256);
}

contract Types is IDataTypesPractice {
    int256 _int256 = 57896044618658097711785492504343953926634992332820282019728792003956564819967;
    uint256 _uint256 = 115792089237316195423570985008687907853269984665640564039457584007913129639935;
    int8 _int8 = 127;
    uint8 _uint8 = 255;
    bool _bool = true;
    address _address = address(this);
    bytes32 _bytes32 = "string";
    uint256[5] uint5arr = [1, 2, 3, 4, 5];
    uint256[] uintArr = [1, 2, 3];
    string str = "Hello World!";

    function getInt256() external view returns(int256) {
        return _int256;
    }

    function getUint256() external view returns(uint256) {
        return _uint256;
    }

    function getIint8() external view returns(int8) {
        return _int8;
    }

    function getUint8() external view returns(uint8) {
        return _uint8;
    }

    function getBool() external view returns(bool) {
        return _bool;
    }

    function getAddress() external view returns(address) {
        return _address;
    }

    function getBytes32() external view returns(bytes32) {
        return _bytes32;
    }

    function getArrayUint5() external view returns(uint256[5] memory) {
        return uint5arr;
    }

    function getArrayUint() external view returns(uint256[] memory) {
        return uintArr;
    }

    function getString() external view returns(string memory) {
        return str;
    }

    function getBigUint() external pure returns(uint256) {
        uint256 v1 = 1;
        uint256 v2 = 2;

        return v2 ** (v1 << (v2 + v2*v2));

    }

}